# NodeJS - Express - MongoDB Example

Run this project by this command : 

1. `npm install`
2. `npm start`
3. Open your favorite browser then type : `http://localhost:3000/`
3. Open your favorite browser then type : `http://localhost:3000/employees`

### Screen shot

Express Home Page

![Express Home page](img/express.png)

Employee Home Page

![Employee Home Page](img/home.png)

Add Employee Page

![Add Employee Page](img/add.png)

Details of an Employee Page

![Details of an Employee Page](img/details.png)

List Employees Page

![List Employees Page](img/list.png)