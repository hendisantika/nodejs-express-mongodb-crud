var express = require('express');
var router = express.Router();

var employee = require("../controllers/EmployeeController.js");

// Get all employees
router.get('/', employee.list);

// Get single employees by id
router.get('/show/:id', employee.show);

// Create employees
router.get('/create', employee.create);

// Save employees
router.post('/save', employee.save);

// Edit employees
router.get('/edit/:id', employee.edit);

// Edit update
router.post('/update/:id', employee.update);

// Edit update
router.post('/delete/:id', employee.delete);

module.exports = router;